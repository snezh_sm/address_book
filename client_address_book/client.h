#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QScopedPointer>

#include <QTcpSocket>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>

class Client :  public QObject
{
    Q_OBJECT
public:
    Client(QObject* parent = nullptr);
    void connectToServer(QString hostname, quint16 port);

    void requestAllRecords();
    void removeRecord(int id);
    void addRecord(QStringList record);
    void setFileForDB(const QStringList& file);
    void recordChanged(const QStringList& record);

signals:
    void statusChanged(QString message);
    void connected();
    void gotAllRecords(const QVector<QStringList>& records);
    void removedRecord(int id);
    void addNewRecord(const QStringList& record);
    
public slots:
    void socketReady();
    void socketDisconnect();

private:
    void sendRequest(const QJsonObject& requset);
    QStringList stringListFromJsonObject(const QJsonObject& obj);

    QScopedPointer<QTcpSocket> m_socket;
};

#endif // CLIENT_H
