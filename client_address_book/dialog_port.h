#ifndef DIALOG_PORT_H
#define DIALOG_PORT_H

#include <QDialog>

namespace Ui {
class Dialog_port;
}

class Dialog_port : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_port(QWidget *parent = nullptr);
    ~Dialog_port();

signals:
    void getConnectionSettings(const QStringList& data);

private slots:
    void on_pb_connect_clicked();

private:
    Ui::Dialog_port *ui;
};

#endif // DIALOG_PORT_H
