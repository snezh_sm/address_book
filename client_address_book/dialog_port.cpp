#include "dialog_port.h"
#include "ui_dialog_port.h"

Dialog_port::Dialog_port(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_port)
{
    ui->setupUi(this);
    setWindowTitle("Connect");
}

Dialog_port::~Dialog_port()
{
    delete ui;
}

void Dialog_port::on_pb_connect_clicked()
{
    QStringList new_connection;
    new_connection << ui->le_localhost->text()
                   << ui->le_port->text();

    emit getConnectionSettings(new_connection);
    Dialog_port::close();
}
