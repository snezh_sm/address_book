#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "table_model.h"
#include "dialog_port.h"

#include <QMainWindow>

#include <QStandardItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pb_add_record_clicked();
    void customMenuRequested(QPoint pos);
    void removeRecord();
    void getConnectionsSet(const QStringList& data);
    void on_actionOpen_triggered();
    void on_actionConnect_triggered();
    void on_getMessage(QString message);

private:

    void setValidator();

    Ui::MainWindow* m_ui;
    TableModel m_table_model;
};
#endif // MAINWINDOW_H
