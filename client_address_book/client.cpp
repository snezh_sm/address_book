#include "client.h"

Client::Client(QObject* parent)
    : QObject(parent)
    , m_socket(new QTcpSocket)
{
    connect(m_socket.get(), &QTcpSocket::connected, this, &Client::connected);
    connect(m_socket.get(), &QTcpSocket::readyRead, this, &Client::socketReady);
    connect(m_socket.get(), &QTcpSocket::disconnected, this, &Client::socketDisconnect);
}

void Client::connectToServer(QString hostname, quint16 port)
{
    //m_socket->connectToHost("127.0.0.1", 2743);
    m_socket->connectToHost(hostname, port);
}

void Client::requestAllRecords()
{
    QJsonObject requst;
    requst["request_type"] = "get_all_records";
    sendRequest(requst);
}

void Client::removeRecord(int id)
{
    QJsonObject requst;
    requst["request_type"] = "remove_record";
    requst["record_id"] = id;
    sendRequest(requst);
}

void Client::addRecord(QStringList record)
{
    if (record.size() < 5)
    {
        qDebug() << "Client: Incorrect new record size:" << record.size();
        return;
    }

    QJsonObject json_record;
    json_record["second_name"] = record[0];
    json_record["name"] = record[1];
    json_record["patronymic"] = record[2];
    json_record["sex"] = record[3];
    json_record["phone"] = record[4];

    QJsonObject requst;
    requst["request_type"] = "add_record";
    requst["new_record"] = json_record;

    sendRequest(requst);
}

void Client::setFileForDB(const QStringList &file)
{
    QJsonObject answer;
    answer["request_type"] = "file_path";
    answer["path"] = file.first();
    m_socket->write(QJsonDocument(answer).toJson());
}

void Client::recordChanged(const QStringList &record)
{
    if (record.size() < 5)
    {
        qDebug() << "Client: Incorrect edit record size:" << record.size();
        return;
    }

    QJsonObject json_record;
    json_record["id"] = record[0];
    json_record["second_name"] = record[1];
    json_record["name"] = record[2];
    json_record["patronymic"] = record[3];
    json_record["sex"] = record[4];
    json_record["phone"] = record[5];

    QJsonObject answer;
    answer["request_type"] = "edit_record";
    answer["record"] = json_record;
    m_socket->write(QJsonDocument(answer).toJson());
}

void Client::socketReady()
{
    // m_socket->waitForReadyRead(500);

    QByteArray data = m_socket->readAll();
    QJsonParseError err;

    QJsonDocument doc = QJsonDocument::fromJson(data, &err);
    if (err.error != QJsonParseError::NoError)
    {
        emit statusChanged("Error with data format");
        return;
    }

    auto request = doc.object();
    QString request_type = request.value("type").toString();

    if (request_type == "connect")
    {
        if (request.value("status").toString() == "yes")
            emit statusChanged("Connection has been established");
        else
            emit statusChanged("Connection has not been established. Try again.");
    }
    else if (request_type == "get_all_records")
    {
        qDebug() << "Client: got all records";
        QJsonArray array = doc.object().value("all_records").toArray();

        QVector<QStringList> records{};
        for(int i = 0; i < array.count(); i++)
            records.push_back(stringListFromJsonObject(array[i].toObject()));

        emit gotAllRecords(records);
    }
    else if (request_type == "add_new_record")
    {
        if (request.value("status").toString() == "yes")
        {
            QJsonObject new_record = request.value("record").toObject();
            QStringList list = stringListFromJsonObject(new_record);
            emit addNewRecord(list);

            qDebug() << "Client: Success with adding record";
        }
        else if (request.value("status").toString() == "no")
        {
            qDebug() << "Client: Error with adding record";
        }
    }
    else if (request_type == "remove_record")
    {
        if (request.value("status").toString() == "yes")
        {
            qDebug() << "Client: Success with removing record";
            int id = request.value("id").toInt();
            emit removedRecord(id);
        }
        else if (request.value("status").toString() == "no")
        {
            qDebug() << "Client: Error with removing record";
        }
    }
    else if (request_type == "edit_record")
    {
        if (request.value("status").toString() == "yes")
        {
            qDebug() << "Client: Success with editing record.";
        }
        else if (request.value("status").toString() == "no")
        {
            qDebug() << "Client: Error with edining record.";
        }
    }
    else
    {
        qDebug() << "Client: Error request from server: " << request_type;
    }
}

void Client::socketDisconnect()
{
    m_socket->deleteLater();
}

void Client::sendRequest(const QJsonObject &requset)
{
    qDebug() << "Socket state: " << m_socket->state();
    auto bytes = QJsonDocument(requset).toJson();
    m_socket->write(bytes);
}

QStringList Client::stringListFromJsonObject(const QJsonObject &obj)
{
    QStringList list = {QString::number(obj.value("id").toInt()),
                        obj.value("second_name").toString(),
                        obj.value("name").toString(),
                        obj.value("patronymic").toString(),
                        obj.value("sex").toString(),
                        obj.value("phone").toString()};

    return list;
}
