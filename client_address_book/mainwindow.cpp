#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QRegExpValidator>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);

    m_ui->tableView->setModel(&m_table_model);
    m_ui->tableView->horizontalHeader()->setVisible(true);
    m_ui->tableView->setColumnHidden(0, true);
    m_ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    setValidator();

    m_ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_ui->tableView, &QTableView::customContextMenuRequested, this, &MainWindow::customMenuRequested);

    m_ui->tableView->show();

    connect(&m_table_model, &TableModel::status, this, &MainWindow::on_getMessage);
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::on_pb_add_record_clicked()
{
    QStringList record;
    record << m_ui->le_second_name->text()
           << m_ui->le_name->text()
           << m_ui->le_patronymic->text()
           << m_ui->le_sex->text()
           << m_ui->le_phone->text();

    m_table_model.addRecord(record);

    m_ui->le_name->clear();
    m_ui->le_second_name->clear();
    m_ui->le_patronymic->clear();
    m_ui->le_sex->clear();
    m_ui->le_phone->clear();
}

void MainWindow::customMenuRequested(QPoint pos)
{
    QMenu* menu = new QMenu(this);
    QAction* delete_p = new QAction("Delete", this);
    connect(delete_p, &QAction::triggered, this, &MainWindow::removeRecord);
    menu->addAction(delete_p);
    menu->popup(m_ui->tableView->viewport()->mapToGlobal(pos));
}

void MainWindow::removeRecord()
{
    int row = m_ui->tableView->selectionModel()->currentIndex().row();
    if (row < 0)
        qDebug() << "MainWindow: Errow with selecting row ";

    if (QMessageBox::warning(this, "Delete record", "Do you want to delete record?",
                             QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
    {
        m_table_model.removeRecord(row);
        qDebug() << "MainWindow: remove record.";
    }
}

void MainWindow::getConnectionsSet(const QStringList &data)
{
    m_table_model.getConnectionSettings(data);
}

void MainWindow::on_actionOpen_triggered()
{
    QFileDialog file_dialog(this);
    file_dialog.setNameFilter("JSON (*.json)");

    QStringList file_name;
    if (file_dialog.exec())
    {
        file_name = file_dialog.selectedFiles();
        m_table_model.setFileForDataBase(file_name);
    }
     else
    {
        qDebug() << "MainWindow: Invalid file for data base.";
        m_table_model.setFileForDataBase(QStringList());
    }
}

void MainWindow::on_actionConnect_triggered()
{
   Dialog_port dialog(this);
   dialog.setModal(true);
   dialog.show();
   connect(&dialog, &Dialog_port::getConnectionSettings, this, &MainWindow::getConnectionsSet);

   dialog.exec();
}

void MainWindow::on_getMessage(QString message)
{
    if (message == "Connection has been established")
        QMessageBox::information(this, "Connection", "Connection has been established.");
    else
        QMessageBox::information(this, "Information", message);
}

void MainWindow::setValidator()
{
    QRegExp for_string("[A-Z]{1}[A-Z,a-z]{25}");
    QValidator* validator_for_string = new QRegExpValidator(for_string, this);

    m_ui->le_name->setValidator(validator_for_string);
    m_ui->le_second_name->setValidator(validator_for_string);
    m_ui->le_patronymic->setValidator(validator_for_string);
    m_ui->le_sex->setValidator(validator_for_string);

    QRegExp for_phone("[+]?([0-9]+[-]?){3,}");
    QValidator* validator_for_phone = new QRegExpValidator(for_phone, this);

    m_ui->le_phone->setValidator(validator_for_phone);
}
