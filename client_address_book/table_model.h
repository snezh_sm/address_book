#ifndef MODEL_H
#define MODEL_H

#include "client.h"

#include <QAbstractTableModel>
#include <QString>
#include <QVector>

using Table = QVector<QStringList>;
//using dataBase = QVector<QStringList>;

class TableModel: public QAbstractTableModel
{
    Q_OBJECT

public:
    TableModel(QObject* parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    void addRecord(QStringList& record);
    void removeRecord(int row);

//    bool insertRows(int position, int rows, const QModelIndex &index=QModelIndex()) override;
//    bool removeRows(int position, int rows, const QModelIndex &index=QModelIndex()) override;

    void setFileForDataBase(const QStringList& file);
    void getConnectionSettings(const QStringList& list);

signals:
    void status(QString message);

public slots:
    void onClientConnected();
    void onClientGotAllRecord(const Table& records);
    void onClientAddRecord(const QStringList& record);
    void onClientRemoveRecord(int id);

private:
    bool validate(int column, const QVariant& value);


    Table m_table;
    QStringList m_headers;
    Client m_client;
};

#endif // MODEL_H
