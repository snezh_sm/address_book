#include "table_model.h"

#include <QRegExpValidator>

static const int COLUMN_COUNT = 6;

TableModel::TableModel(QObject *parent)
    :QAbstractTableModel(parent)
{
    m_headers << "Id" << "Surname" << "Name" << "Patronymic" << "Gender" << "Phone";

    connect(&m_client, &Client::connected, this, &TableModel::onClientConnected);
    connect(&m_client, &Client::statusChanged, this, &TableModel::status);
    connect(&m_client, &Client::gotAllRecords, this, &TableModel::onClientGotAllRecord);
    connect(&m_client, &Client::addNewRecord, this, &TableModel::onClientAddRecord);
    connect(&m_client, &Client::removedRecord, this, &TableModel::onClientRemoveRecord);

    //m_client.connectToServer("127.0.0.1", 2743);
}

int TableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_table.size();
}

int TableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_headers.size();
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    auto parent_flags = QAbstractTableModel::flags(index);

    if (!index.isValid())
        return parent_flags;

    return parent_flags | Qt::ItemIsEditable;
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{   
    if (role != Qt::DisplayRole)
        return QVariant();

    if (!index.isValid())
        return QVariant();

    if (index.row() >= rowCount() || index.column() >= columnCount())
        return QVariant();

    return m_table.at(index.row()).at(index.column());
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole || index.row() >= rowCount() ||
            index.column() >= columnCount())
        return false;

    if (!validate(index.column(), value))
        return false;

    auto new_record = m_table[index.row()];
    new_record[index.column()] = value.toString();
    m_table[index.row()] = new_record;
    m_client.recordChanged(new_record);

    emit layoutChanged();
    return true;
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || section > columnCount())
        return QVariant();

    if (orientation == Qt::Horizontal)
        return m_headers[section];

    return QVariant();
}

void TableModel::addRecord(QStringList &record)
{
    m_client.addRecord(record);
}

void TableModel::removeRecord(int row)
{
    if (row < 0 || row >= m_table.size())
    {
        qDebug() << "TableModel: Invadid row to remove.";
        return;
    }

    auto record_to_remove = m_table[row];
    m_client.removeRecord(record_to_remove.first().toInt());
}

void TableModel::setFileForDataBase(const QStringList &file)
{
    if (file.isEmpty())
    {
        qDebug() << "TableModel: No file selected.";
        return;
    }

    m_client.setFileForDB(file);
}

void TableModel::getConnectionSettings(const QStringList &list)
{
    QString localhost = list.first();
    quint16 port = static_cast<quint16>(list.last().toInt());
    m_client.connectToServer(localhost, port);
}

void TableModel::onClientConnected()
{
    //m_client.requestAllRecords();
}

void TableModel::onClientGotAllRecord(const Table& records)
{
    m_table = records;
    emit layoutChanged();
    //qDebug() <<"Table: " << m_table;
}

void TableModel::onClientAddRecord(const QStringList& record)
{
    m_table.push_back(record);
    emit layoutChanged();
}

void TableModel::onClientRemoveRecord(int id)
{
    for (int i = 0; i < m_table.size(); i++)
    {
        if (m_table[i].first().toInt() == id)
            m_table.removeAt(i);
    }
    emit layoutChanged();
}

bool TableModel::validate(int column, const QVariant &value)
{
    if (column > 0 && column < 5)
    {
        QString str = value.toString();
        for (auto& i : str)
        {
            if (i < 'A' || i > 'z')
                return false;
        }
        return true;
    }
    else if (column == 5)
    {
        QString str = value.toString();
        if (str.size() < 4)
            return false;

        for (int i = 0; i < str.size(); i++)
        {
           if (i != 0 && str[i] == '-')
               return false;

           if (str[i] != '+' && (str[i] < '0' || str[i] > '9'))
               return false;
        }
        return true;
    }

    return false;
}
