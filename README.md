# Run SERVER

To build SERVER do this:

1. Create the build output directory
2. CD to the build output directory
3. Call qmake referencing the server_address_book.pro file
4. Call make

To run SERVER do this: In build directory call 

    ./server_address_book hostname port
For example, 
    
    ./server_address_book localhost 2743

# Run CLIENT
To build CLIENT see abov how to build SERVER.
To run do this: In build directory call 
       
       ./client_address_book

## CLIENT: what to do

- First of all you need to connect to server. Press `"Server"` in the upper left corner, then press `"Connect"`. In dialog window enter hostname and port (tha same as in SERVER), then press `"Connect"`. If connection is successful, you see message `"Connection has been established"`. Otherwise something is wrong. Try again!
- The second is to select data base (.json file). Press `"Data base"` in the upper left corner, then press `"Open"`, then choose a file in file system. You can use "~/server_address_book/address_book.json" or create another file (with {..} inside).


| What you want | How to do |
| ------ | ------ |
| Add new record| Fill all field on the right and press `Add record`
| Remove record| On selected record call context menu and press `Delete`. Сonfirm or deny.
| Edit record| Double press in selected cell and fill field. Then press Enter.


