#ifndef DATABASE_H
#define DATABASE_H

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>

#include <QVector>
#include <QStringList>

#include <QFile>


class DataBase
{
public:
    ~DataBase();

    QJsonArray getAllRecords();
    bool removeRecord(int id);
    bool addRecord(QByteArray record);
    bool editRecord(QByteArray record);

    QJsonValue getNewRecord();

    bool open(QString file_path);
    void close();

private:
    int getIndex(int id);
    bool writeToFile();
    int getMaxId();

    QJsonArray m_doc_array;
    QFile m_file;
};

#endif // DATABASE_H
