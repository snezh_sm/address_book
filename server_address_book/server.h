#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QScopedPointer>

class DataBase;

class server: public QTcpServer
{
    Q_OBJECT
public:
    server();
    ~server();

public slots:
    void startServer(QString hostname, int port);
    void onNewConnection();
    void socketReady();
    void socketDisconnected();

private:
    QScopedPointer<DataBase> m_data_base;
};

#endif // SERVER_H
