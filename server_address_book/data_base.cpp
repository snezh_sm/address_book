#include "data_base.h"

#include <QDebug>

static const QString FILE_NAME = "address_book.json";

DataBase::~DataBase()
{
    close();
}

QJsonArray DataBase::getAllRecords()
{
    return m_doc_array;
}

bool DataBase::removeRecord(int id)
{
    int idx = getIndex(id);
    if (idx == -1)
    {
        qDebug() << "DataBase: Invalid index for removing record.";
        return false;
    }

    m_doc_array.removeAt(idx);
    if (!writeToFile())
    {
        qDebug() << "DataBase: Error while writing to file.";
        return false;
    }

    return true;
}

bool DataBase::addRecord(QByteArray record)
{
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(record, &err);
    if (err.error != QJsonParseError::NoError)
    {
        qDebug() << "DataBase: Error with data format in message from client. " + err.errorString();
        m_file.close();
        return false;
    }

    QJsonObject new_record = doc.object();
    //int id = m_doc_array.last().toObject().value("id").toInt() + 1;
    int id = getMaxId() + 1;
    new_record.insert("id", id);

    m_doc_array.append(new_record);

    if (!writeToFile())
    {
        qDebug() << "DataBase: Error while writing to file.";
        return false;
    }

    return true;
}

bool DataBase::editRecord(QByteArray record)
{
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(record, &err);
    if (err.error != QJsonParseError::NoError)
    {
        qDebug() << "DataBase: Error with data format in message from client. " + err.errorString();
        m_file.close();
        return false;
    }

    QJsonObject record_to_edit = doc.object();
    QString id_Str = record_to_edit.value("id").toString();
    int id = id_Str.toInt();

    if (!removeRecord(id))
    {
        qDebug() << "Data base: Error with edit record (remove).";
        m_file.close();
        return false;
    }

    record_to_edit.insert("id", id);
    m_doc_array.append(record_to_edit);
    if (!writeToFile())
    {
        qDebug() << "DataBase: Error while writing to file.";
        return false;
    }

    return true;
}

QJsonValue DataBase::getNewRecord()
{
    return m_doc_array.last();
}

bool DataBase::open(QString file_path)
{
    m_file.setFileName(file_path);

    if (!m_file.exists())
    {
        qDebug() << "DataBase: File doesn't exist.";
        return false;
    }

    if (!m_file.open(QIODevice::ReadWrite | QFile::Text))
    {
        qDebug() << "DataBase: Error with opening file: " << m_file.error();
        return false;
    }

    QByteArray from_file = m_file.readAll();
    QJsonParseError err;
    auto doc = QJsonDocument::fromJson(from_file, &err);

    if (err.error != QJsonParseError::NoError)
    {
        qDebug() << "DataBase: Error with data format while reading from file. " + err.errorString();
        m_file.close();
        return false;
    }

    m_doc_array = doc.object().value("members").toArray();

    return true;
}

void DataBase::close()
{
    m_file.close();
}

int DataBase::getIndex(int id)
{
    for (int i = 0; i < m_doc_array.size(); i++)
    {
        if (m_doc_array[i].toObject().value("id").toInt() == id)
            return i;
    }

    return -1;
}

bool DataBase::writeToFile()
{
    QJsonObject members;
    members["members"] = m_doc_array;

    m_file.resize(0);
    m_file.write(QJsonDocument(members).toJson());
    m_file.flush();

    return true;
}

int DataBase::getMaxId()
{
    int id = -1;
    for (const auto& record : m_doc_array)
    {
        if (record.toObject().value("id").toInt() > id)
            id = record.toObject().value("id").toInt();
    }

    return id;
}
