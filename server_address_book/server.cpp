#include "server.h"
#include "data_base.h"

#include <QDataStream>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>

server::server()
    : m_data_base(new DataBase)
{
    connect(this, &QTcpServer::newConnection, this, &server::onNewConnection);
}

server::~server()
{}

void server::startServer(QString hostname, int port)
{
    //if (listen(QHostAddress::Any, 2743))
    if (listen(static_cast<QHostAddress>(hostname), port))
        qDebug() << "Server: Listening...";
    else
        qDebug() << "Server: Not listening...";
}

void server::onNewConnection()
{
    QTcpSocket* socket = nextPendingConnection();
    connect(socket, &QTcpSocket::readyRead, this, &server::socketReady);
    connect(socket, &QTcpSocket::disconnected, this, &server::socketDisconnected);

    qDebug() << "Server: Client connected";
    socket->write("{\"type\":\"connect\", \"status\":\"yes\"}");
}

void server::socketReady()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    QByteArray data = socket->readAll();
    //qDebug() << "Server: Message from client: " << data;

    QJsonParseError err;
    QJsonDocument document = QJsonDocument::fromJson(data, &err);

    if (err.error != QJsonParseError::NoError)
    {
        qDebug() << "Server: Error with data format. " + err.errorString();
        return;
    }

    auto request = document.object();
    QString request_type = request.value("request_type").toString();

    if (request_type == "file_path")
    {
        QString path = request.value("path").toString();
        if (!m_data_base->open(path))
        {
            qDebug() << "Server: Error with opening data base.";
            return;
        }
        qDebug() << "Server: Success with opening data base.";

        QJsonObject answer;
        answer["type"] = "get_all_records";
        answer["all_records"] = m_data_base->getAllRecords();

        socket->write(QJsonDocument(answer).toJson());
        qDebug() << "Server: Get all records from file.";
    }
    else if (request_type == "get_all_records")
    {
        QJsonObject answer;
        answer["type"] = "get_all_records";
        answer["all_records"] = m_data_base->getAllRecords();

        socket->write(QJsonDocument(answer).toJson());
        qDebug() << "Server: Get all records from file.";
    }
    else if (request_type == "add_record")
    {
        QJsonObject record = request.value("new_record").toObject();
        qDebug() << "Server: Got a new record.";

        if (m_data_base->addRecord(QJsonDocument(record).toJson()))
        {            
            QJsonObject answer;
            answer["type"] = "add_new_record";
            answer["status"] = "yes";
            answer["record"] = m_data_base->getNewRecord();

            socket->write(QJsonDocument(answer).toJson());
            qDebug() << "Server: Add new record to data base.";
        }
        else
        {
            qDebug() << "Server: Error with adding new record.";
            QJsonObject answer;
            answer["type"] = "add_new_record";
            answer["status"] = "no";
            socket->write(QJsonDocument(answer).toJson());
        }
    }
    else if (request_type == "remove_record")
    {
        int record = request.value("record_id").toInt();
        qDebug() << "Got record id to remove from client.";

        if (m_data_base->removeRecord(record))
        {
            qDebug() << "Server: Remove record from data base.";
            QJsonObject answer;
            answer["type"] = "remove_record";
            answer["status"] = "yes";
            answer["id"] = record;
            socket->write(QJsonDocument(answer).toJson());
        }
        else
        {
            qDebug() << "Server: Error with removing new record.";
            QJsonObject answer;
            answer["type"] = "remove_record";
            answer["status"] = "no";
            socket->write(QJsonDocument(answer).toJson());
        }
    }
    else if (request_type == "edit_record")
    {
        QJsonObject record = request.value("record").toObject();
        qDebug() << "Server: Got a record ro edit.";
        if (m_data_base->editRecord(QJsonDocument(record).toJson()))
        {
            QJsonObject answer;
            answer["type"] = "edit_record";
            answer["status"] = "yes";
            socket->write(QJsonDocument(answer).toJson());

            qDebug() << "Server: Succesful editing a record in data base.";
        }
        else
        {
            qDebug() << "Server: Error with editing new record.";
            QJsonObject answer;
            answer["type"] = "edit_record";
            answer["status"] = "no";
            socket->write(QJsonDocument(answer).toJson());
        }
    }
    else
    {
        qDebug() << "Server: Unknown type request:" << request_type;
    }
}

void server::socketDisconnected()
{
    QObject* obj = sender();
    obj->deleteLater();
    m_data_base->close();
    qDebug() << "Server: Client disconnected.";
}
