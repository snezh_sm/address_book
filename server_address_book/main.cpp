#include "server.h"
#include "data_base.h"

#include <QCoreApplication>
#include <QCommandLineParser>

int to_int(char* str)
{
    int res = 0;
    for (int i = 0; str[i] != '\0'; ++i)
        res = res * 10 + str[i] - '0';
    return res;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString hostname = argv[1];
    int port = to_int(argv[2]);

    if (port < 1000 || port > 10000)
    {
        qDebug() << "Invalid port.";
        return -1;
    }

    server server;
    server.startServer(hostname, port);

    return a.exec();
}
